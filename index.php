<?php

$visite = json_decode($_COOKIE["visite"]);

if ($visite) {
    $visite[] = date("d/m/y - H:i:s");
} else {
    $visite = array(date("d/m/y - H:i:s"));
}

setcookie("visite", json_encode($visite), strtotime("+10 year"), "", "", true, true);

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>visite</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            background: black;
        }
        .container {
            margin: 10%;
        }
        .box {
            border: 1px solid black;
            background: #FCA502;
            color: black;
            margin: 0 auto;
            padding: 10px;
            text-align: center;
            width: 200px;
            height: auto;
        }
        .box #visite-precedenti {
            border: 1px solid black;
            overflow: auto;
            height: 200px;
        }
        #delete-cookies-button {
            border-radius: 1px;
            border: 1px solid black;
        }
    </style>
</head>
<body>
<div class="container">
<!--    se è la prima visita stampa il benvenuto    -->
    <?php if(count($visite) == 1){ ?>
        <div class="box">
            <h2>Benvenuto</h2>
        </div>
    <?php } else { ?>

    <div class="box">
        <h2>Visite precedenti:</h2>
        <div id="visite-precedenti">
            <?php foreach ($visite as $visita) { ?>
                <p><?php if (end($visite) != $visita) echo $visita ?></p>
            <?php } ?>
        </div>
        <h2>Visita attuale:</h2>
        <p><?= end($visite) ?></p>
        <form action="deleteCookies.php" method="get">
            <button type="submit" id="delete-cookies-button">cancella i cookies</button>
        </form>
    </div>

    <?php } ?>
</div>
<script>

</script>
</body>
</html>
