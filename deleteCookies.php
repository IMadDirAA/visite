<?php

foreach ($_COOKIE as $key => $cookie) {
    setcookie($key, "", time() - 3600);
}

if (isset($_SERVER["HTTP_REFERER"])) {
    header("location: " . $_SERVER["HTTP_REFERER"]);
    http_response_code(302);
} else {
    http_response_code(200);
    echo "<h1>cookies deleted</h1>";
}
